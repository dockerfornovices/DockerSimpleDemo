FROM mongo:4

RUN useradd -m developer

WORKDIR /development

USER developer

CMD ["/bin/bash"]

ARG SOURCE
ARG CREATED
LABEL org.opencontainers.image.source="${SOURCE}"
LABEL org.opencontainers.image.created="${CREATED}"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.description="A demo image that provides Lua"
